# Car96 MCU firmware

This firmware is compatible with Arduino IDE.

There are two variants;
1) Car96-MCU-NOCDC
2) Car96-MCU-CDC

The second version enabled the USB CDC port (virtal serial port) for additional
debugging if needed. Wipe and reboot-bootloader, which is normally activated by
opening the CDC port at 1200 bps (stty -F /dev/ttyACMx 1200) can be performed on
the no-CDC version by sending command 0xFF to the appropriate hidraw device.

The appropriate hidraw device can be determined by the response to the command
0x20, which will be 0x01, or by sending the command to all hidraw devices, only
the correct hidraw device will respond to that command.

When updating the Car96 MCU firmware on-device, press "Ctrl-Alt-S" to export
compiled firmware "SWI.ino.car96mcu.bin". This file can be written via
CarSettings application, or from the commandline via "bossac" using this
command;

```
bossac -i -d --port=/dev/ttyACMx -U -i --offset=0x2000 -e -w -v SWI.ino.car96mcu.bin -R
```

However note the two caveats regarding writing the firmware using bossac;
1) If gpsd is running, it could restrict access to the serial port. It is best
to kill gpsd first.
2) bossac is unable to send the wipe-and-reboot command if the MCU is running
a non-CDC build, in which case, it will be necessary to reset it by "double
reset" using the reset pin available on the board itself, OR, by writing a
simple program to issue the wipe command.

Setup Arduino IDE as follows;
File --> Preferences, and locate the dialog "Additional Boards Manager URLs:"
and add to it the following;
```
https://gitlab.com/HiKey960-Car/mcu_firmware/raw/master/package_car96mcu_index.json
```
Press OK when added, and continue as follows;
Tools --> Board --> Boards Manager.
Wait for it to update, then enter "Car96" in the search box. Select the highest
version and press "Install". You can then close the boards manager and select
the board.

# Programming a blank Car96 MCU;
If it happens that you have a blank MCU without a bootloader, then you will have
to write a bootloader to it in order to use it normally.

These instructions for loading the bootloader are fairly easy to follow, and
do not requre any particularly specialized hardware. Just a SAMD21 breakout
board with an SDCARD socket: https://learn.adafruit.com/programming-an-m0-using-an-arduino/overview

Important: DO NOT connect the 3.3v pins. Instead, power on the Car96. Most
SAMD21 breakout boards will not provide enough power to supply the 3.3v rail
of the Car96.